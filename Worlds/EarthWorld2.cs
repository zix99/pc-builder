using System;
using pluginbase.Attributes;
using pluginbase.Objects.World.Generators;
using pluginbase.Objects.World;
using pluginbase.Objects.World.Blocks;
using pluginbase.Helpers.Computative;
using pluginbase.Dependencies;
using pluginbase.Helpers.Computative.Algorithms;
using essentials.Blocks.Earth;
using essentials.Blocks.Liquid;
using essentials.Blocks.Geological;
using pluginbase.Helpers.Computative.Random;
using essentials.blocks.vegetation.Trees;
using essentials.blocks.vegetation.Brush;
using essentials.blocks.vegetation.Underwater;

namespace builder.Worlds
{
    [WorldGenerator("EarthWorld2", Author = "Pontoon City", Description = "Earth-like world v2", IconPath = "icons/islands.png")]
    [Config("waterlevel", "Water level", 0)]
    [Config("worldsize", "World size before ice", 8000)]
    [Config("cavecount", "Cave Detail", 2)]
    public class EarthWorld2 : WorldGeneratorBase
    {
        private readonly IBlockLookup _blocks;

        private readonly ValuePoint3D _mountainsHeight;
        private readonly Noise _mountainsTrees;
        private readonly ValuePoint3D _mountainsTreeHeight;
        private readonly ValuePoint3D _mountainsCarved;

        private readonly ValuePoint3D _desertHeight;
        private readonly ValuePoint3D _desertBiome;
        private readonly Noise _desertCactus;
        private const int DESERT_MAX_HEIGHT = 32;

        private readonly ValuePoint3D _swampBiome;
        private readonly ValuePoint3D _swampHeight;
        private readonly ValuePoint3D _swampGroundType;
        private readonly Noise _swampTrees;

        private readonly ValuePoint3D _tundraHeight;

        private readonly ValuePoint3D _globalBrush;

        private readonly int _caveCount = 0;
        private readonly ValuePoint3D [] _caves;
        private readonly ValuePoint3D [] _caveHeight;


        private readonly int _waterlevel;
        private readonly int _worldsize;

        [Dependency]
        private readonly ISettings _settings;

        public EarthWorld2 (int seed, IBlockLookup blockLookup)
        {
            this.InjectDependencies ();
            _blocks = blockLookup;

            _waterlevel = _settings.Get ("waterlevel", 0);
            _worldsize = _settings.Get ("worldsize", 8000);

            _mountainsHeight = new SmoothPerlin (seed, 128, 6).Gamma (0.5f);
            _mountainsTrees = new Noise (seed+1, 0.008f);
            _mountainsTreeHeight = new Random3D (seed + 2);
            _mountainsCarved = new SmoothPerlin (seed + 3, 16, 3);

            _desertBiome = new Perlin (seed + 1, 512, 1);
            _desertHeight = new SmoothPerlin (seed+4, 64, 2);
            _desertCactus = new Noise (seed + 5, 0.002f);

            _swampHeight = (new SmoothPerlin (seed + 10, 16, 3) - new SmoothPerlin (seed + 11, 16, 3)).Abs ().Gamma(3f);
            _swampBiome = new Perlin (seed + 11, 512, 1);
            _swampGroundType = new Perlin (seed + 12, 64, 2);
            _swampTrees = new Noise (seed + 13, 0.01f);

            _tundraHeight = new SmoothPerlin (seed + 24, 32, 2);

            _globalBrush = new Random3D (seed + 100);

            _caveCount = _settings.Get ("cavecount", 0);
            _caves = new ValuePoint3D [_caveCount];
            _caveHeight = new ValuePoint3D [_caveCount];
            for (int i=0; i<_caveCount; i++)
            {
                _caves[i] = (new SmoothPerlin (seed + 30 * i, 64, 6) - new SmoothPerlin (seed + 30*i + 1, 64, 6)).Abs ().Invert ().LinearBounds (0.5f, 1f).Gamma (0.1f);
                _caveHeight [i] = new SmoothPerlin (seed + 30 * i + 2, 256, 2);
            }
        }

        public override IBlock GetBlock (int x, int y, int z)
        {
            float desertWeight = MExt.NormalizeClamp (_desertBiome.GetValue (x, y), 0.45f, 0.65f);
            float swampWeight = MExt.NormalizeClamp (_swampBiome.GetValue (x, y), 0.45f, 0.65f);
            float tundraWeight = MExt.NormalizeClamp (Math.Abs (x) + Math.Abs (y), _worldsize, _worldsize + 100);
            bool isDesert = desertWeight > 0.5f;
            bool isSwamp = swampWeight > 0.5f;
            bool isTundra = tundraWeight > 0.3f;

            //Get height interpolating for all biomes
            int height = 0;
            if (tundraWeight < 1f || swampWeight < 1f || desertWeight < 1f)
                height = _mountainsHeight.GetValue (x, y, -32, 128);
            if (tundraWeight > 0f)
                height = (int)Interpolate.SmoothStep (height, _tundraHeight.GetValue (x, y, -12, 16), tundraWeight);
            if (swampWeight > 0f) //optimization
                height = (int)Interpolate.SmoothStep (height, _swampHeight.GetValue (x, y, -6, 6), swampWeight);
            if (desertWeight > 0f) //optimization
                height = (int)Interpolate.SmoothStep (height, _desertHeight.GetValue (x, y, _waterlevel+2, DESERT_MAX_HEIGHT), desertWeight);

            if (z < -128)
            {
                return BiomeDeep (height, x, y, z);
            }

            if (height < _waterlevel - 4)
                return BiomeUnderwater (height, x, y, z);

            // Caves
            if (z <= height+10) {
                for (int i = 0; i < _caveCount; i++) {
                    float caveVal = _caves [i].GetValue (x, y);
                    if (caveVal > 0.3f) {
                        float caveHeight = _caveHeight [i].GetValue (x, y, -30, 30);
                        if (z > caveHeight && z <= caveHeight + 4 * caveVal + 3) {
                            return _blocks.EmptyBlock;
                        }
                    }
                }
            }

            if (isTundra)
                return BiomeTundra (height, x, y, z);

            if (isDesert)
                return BiomeDesert (height, x, y, z);

            if (isSwamp)
                return BiomeSwamp (height, x, y, z);

            return BiomeMountains (height, x, y, z);
        }

        private IBlock BiomeTundra(int height, int x, int y, int z)
        {
            if (z < height)
                return _blocks.GetBlock<Snow> ();
            if (z < _waterlevel)
                return _blocks.GetBlock<Ice> ();
            return _blocks.EmptyBlock;
        }

        private IBlock BiomeDesert(int height, int x, int y, int z)
        {
            if (z < height)
                return _blocks.GetBlock<BeachSand> ();

            if (z < _waterlevel)
                return _blocks.GetBlock<InfiniteWater> ();

            if (z >= height && z < height + 5 && _desertCactus.GetState(x,y))
            {
                return _blocks.GetBlock<Cactus> ();
            }

            if (z == height && height < DESERT_MAX_HEIGHT - 10)
            {
                //brush
                switch(_globalBrush.GetValue(x,y,0,50))
                {
                    case 0:
                        return _blocks.GetBlock<DesertPlant> ();
                    case 1:
                        return _blocks.GetBlock<Brush1> ();
                    case 2:
                        return _blocks.GetBlock<Brush2> ();
                    case 3:
                        return _blocks.GetBlock<Brush3> ();
                }
            }

            return _blocks.EmptyBlock;
        }

        private IBlock BiomeUnderwater(int height, int x, int y, int z)
        {
            if (z < height)
                return _blocks.GetBlock<Rock> ();

            if (z < _waterlevel)
            {
                if (z == height)
                {
                    switch(_globalBrush.GetValue(x,y,0,100))
                    {
                        case 0:
                            return _blocks.GetBlock<EelGrass> ();
                    }
                }
                return _blocks.GetBlock<InfiniteWater> ();
            }

            return _blocks.EmptyBlock;
        }

        private IBlock BiomeSwamp(int height, int x, int y, int z)
        {
            const int TREES_ABOVE_WATERLEVEL = 2;

            if (z < height)
            {
                float groundType = _swampGroundType.GetValue (x, y);
                if (groundType > 0.7f)
                    return _blocks.GetBlock<Mud> ();
                if (groundType > 0.5f)
                    return _blocks.GetBlock<Dirt> ();
                return _blocks.GetBlock<Moss>();
            }

            if (z < _waterlevel)
                return _blocks.GetBlock<MirkyWater> ();
            if (z == _waterlevel && height < _waterlevel)
            {
                if (_globalBrush.GetValue (x, y) > 0.9f)
                    return _blocks.GetBlock<Lilypads> ();
            }

            if (z >= height && z < height + 7 && height > _waterlevel + TREES_ABOVE_WATERLEVEL)
            {
                if (_swampTrees.GetState (x, y))
                    return _blocks.GetBlock<PineWood> ();

                if (z >= height + 2 && (
                        _swampTrees.GetState(x+1, y)
                        || _swampTrees.GetState(x-1, y)
                        || _swampTrees.GetState(x,y+1)
                        || _swampTrees.GetState(x,y-1)
                        )
                    )
                {
                    return _blocks.GetBlock<PineBranch> ();
                }
                
            }

            if (z == height)
            {
                //Shrubs
                int brush = _globalBrush.GetValue (x, y, 0, 70);
                switch(brush)
                {
                    case 0:
                        return _blocks.GetBlock<Fern> ();
                    case 1:
                        return _blocks.GetBlock<PalmLeaves> ();
                    case 2:
                        return _blocks.GetBlock<Bush> ();
                    case 3:
                        return _blocks.GetBlock<LargeBush> ();
                    case 4:
                        return _blocks.GetBlock<FlowerYellow> ();
                    case 5:
                        return _blocks.GetBlock<FlowerPurple> ();
                    case 6:
                    case 7:
                    case 8:
                        return _blocks.GetBlock<ReedsTall> ();
                    case 9:
                    case 10:
                    case 11:
                        return _blocks.GetBlock<Reeds> ();
                    case 12:
                    case 13:
                    case 14:
                        return _blocks.GetBlock<GrassBlades> ();
                }
            }

            return _blocks.EmptyBlock;
        }

        private IBlock BiomeMountains(int height, int x, int y, int z)
        {
            const int BEACH_SAND_HEIGHT = 2;
            const int TREE_HEIGHT = 12;
            const int BRANCH_START = 4;
            const int SNOW_HEIGHT = 80;
            const int VOLCANO_HEIGHT = 70;
            const float ROCK_STEEPNESS = 0.01f;

            float relHeight = _mountainsHeight.GetValue (x, y);
            float steepness = Math.Max (
                Math.Abs (_mountainsHeight.GetValue (x, y + 1) - relHeight),
                Math.Abs (_mountainsHeight.GetValue (x + 1, y) - relHeight)
                );

            bool dugOut = _mountainsCarved.GetValue (x, y, z) < 0.3f && z > _waterlevel;

            if (height > VOLCANO_HEIGHT)
            {
                height = VOLCANO_HEIGHT - (height - VOLCANO_HEIGHT); //Inverting
                if (z < height)
                    return _blocks.GetBlock<Rock> ();
                if (z < VOLCANO_HEIGHT - 15)
                    return _blocks.GetBlock<Lava>();
                return _blocks.EmptyBlock;
            }

            //Ground surface
            if (z < height && !dugOut)
            {
                if (z > SNOW_HEIGHT)
                {
                    if (z < height - 2)
                        return _blocks.GetBlock<Rock> ();
                    return _blocks.GetBlock<Snow> ();
                }

                if (steepness > ROCK_STEEPNESS)
                    return _blocks.GetBlock<Rock> ();

                if (z < _waterlevel + BEACH_SAND_HEIGHT)
                    return _blocks.GetBlock<Sand> ();

                if (z < height - 5)
                    return _blocks.GetBlock<Rock> ();
                if (z < height - 2)
                    return _blocks.GetBlock<Dirt> ();
                return _blocks.GetBlock<Grass> ();
            }


            if (height > _waterlevel + BEACH_SAND_HEIGHT && height < SNOW_HEIGHT && steepness <= ROCK_STEEPNESS && !dugOut)
            {
                //Trees
                if (z >= height && z < height + TREE_HEIGHT)
                {
                    if (_mountainsTrees.GetState (x, y))
                        return _blocks.GetBlock<DecidiousWood> ();
                    if (z >= height + BRANCH_START)
                    {
                        if (_mountainsTrees.GetState(x+1,y)
                            || _mountainsTrees.GetState(x-1,y)
                            || _mountainsTrees.GetState(x,y+1)
                            || _mountainsTrees.GetState(x,y-1))
                        {
                            return _blocks.GetBlock<DecidiousBranch> ();
                        }
                    }
                }

                //Brush
                if (z == height)
                {
                    int brush = _globalBrush.GetValue (x, y, 0, 60);
                    switch(brush)
                    {
                        case 0:
                            return _blocks.GetBlock<Bush> ();
                        case 1:
                            return _blocks.GetBlock<LargeBush> ();
                        case 2:
                            return _blocks.GetBlock<FlowerYellow> ();
                        case 3:
                            return _blocks.GetBlock<FlowerPurple> ();
                        case 4:
                            return _blocks.GetBlock<Boulder> ();
                    }
                    if (brush < 15)
                        return _blocks.GetBlock<GrassBlades> ();
                }
            }

            if (height > _waterlevel && height <= _waterlevel + BEACH_SAND_HEIGHT)
            {
                if (_mountainsTrees.GetState(x,y))
                {
                    int treeHeight = _mountainsTreeHeight.GetValue (x, y, 4, 10);
                    if (z >= height && z < height + treeHeight)
                        return _blocks.GetBlock<PalmWood> ();
                    if (z == height + treeHeight)
                        return _blocks.GetBlock<PalmTreeLeaves> ();
                }

                if (z == height)
                {
                    switch(_globalBrush.GetValue(x,y,0,50))
                    {
                        case 0:
                            return _blocks.GetBlock<Reeds> ();
                        case 1:
                            return _blocks.GetBlock<ReedsTall> ();
                        case 2:
                            return _blocks.GetBlock<ReedsExtraTall> ();
                        case 4:
                            return _blocks.GetBlock<PalmLeaves> ();
                    }
                }
            }

            if (z < _waterlevel)
                return _blocks.GetBlock<InfiniteWater> ();

            return _blocks.EmptyBlock;
        }

        private IBlock BiomeDeep(int height, int x, int y, int z)
        {
            return _blocks.GetBlock<Rock> ();
        }
    }
}

