using System;
using pluginbase.Objects.World.Generators;
using pluginbase.Objects.World.Blocks;
using pluginbase.Objects.World;
using pluginbase.Attributes;
using pluginbase.Helpers.Computative.Algorithms;
using pluginbase.Helpers.Computative.Random;

namespace builder.Worlds
{
	[WorldGenerator("Earth", Author = "Pontoon City", IconPath="icons/islands.png")]
	public class EarthWorld : WorldGeneratorBase
	{
		private readonly BlockCache _cache;

		private readonly SmoothPerlin _terrain;
		private readonly Perlin _oceans;

		private readonly Random3D _brushType;
		private readonly Perlin _grassClumps;

		private readonly Perlin _terrainClumps;

        private readonly SmoothPerlin _clouds;

		public EarthWorld (int seed, IBlockLookup lookup)
		{
			//We create a cache-object which is a class with a bunch of fields that define blocks we want to use
			//This is faster and safer compared to using lookup.GetBlock<T> each time 
			_cache = lookup.PopulateCache<BlockCache>();

			//Simplex is a perlin noise alternative used for creating smooth noise.
			//You can read more on this here: https://www.empeld.com/devwiki/deterministic_random_numbers
            _terrain = new SmoothPerlin(seed, 64, 4);
			_oceans = new Perlin(seed, 256, 2);

			_brushType = new Random3D(seed+153);
			_grassClumps = new Perlin(seed+1, 16, 2);

			_terrainClumps = new Perlin(seed+2, 48, 2);

            _clouds = new SmoothPerlin(seed+5, 64, 4);
		}

		const int WATER_LEVEL = -10;
		const int CLOUD_LEVEL = 32;
		const int CAVE_MAX_DEPTH = -30;
		const int CLOUD_LEVEL_TOP = CLOUD_LEVEL + 16;

		public override IBlock GetBlock (int x, int y, int z)
		{
			int terrainHeight = _terrain.GetValue(x,y,0,16) + _oceans.GetValue(x,y,-64,16);

			//If z < the computed terrain height, it must be below-ground, so let's figure out what it is:
			if (z < terrainHeight)
			{
				//If it's not too deep, draw some caves
				if (z > CAVE_MAX_DEPTH)
				{
					if (_terrain.GetValue(x,y,z) < 0.3f)
					{
						if (z < CAVE_MAX_DEPTH+5)
						{
							return _cache.Lava;
						}

						return _cache.Empty;
					}
				}

				//Below surface rocks, at 5 below surface level
				if (z < terrainHeight - 5)
				{
					return _cache.Rock;
				}
				//Dirt at 2 below surface level
				if (z < terrainHeight - 2)
				{
					return _cache.Dirt;
				}

				//Random patches of rocky grass
				if ( _terrainClumps.GetValue(x,y) < 0.15f)
				{
					return _cache.RockyGrass;
				}

				//Near water-level sand
				if (z < WATER_LEVEL + 3)
				{
					return _cache.BeachSand;
				}

				//Otherwise, it must be the top level, which we'll set as grass
				return _cache.Grass;
			}

			//If Z is below our set water level, then it's in the ocean
			if (z < WATER_LEVEL)
			{
				//Let's see if we can apply some underwater items (Boulders, eelgrass, etc)
				if (z == terrainHeight)
				{
					switch(_brushType.GetValue(x,y,0,300))
					{
						case 0:
							return _cache.Boulder;
						case 1:
							return _cache.EelGrass;
					}
				}

				//Otherwise, it's water
				return _cache.Water;
			}

			//If z is == to terrainHeight, that means we're on the surface and we
			//Can get surface features such as bushes.  Once we verify this, we'll
			//use the random noise generate to decide whether to place a bush or not
			if (z == terrainHeight)
			{
				switch(_brushType.GetValue(x,y,0,300))
				{
					case 0:
						return _cache.Boulder;
					case 1:
					case 2:
						return _cache.BushLarge;
					case 4:
					case 5:
					case 6:
						return _cache.Fern;
					case 7:
					case 8:
						return _cache.GrassTuft;
				}

				//This uses a perlin noise so that we get groups of grass tufts rather than individual ones
				if (_grassClumps.GetValue(x,y) < 0.1f)
				{
					if (z < WATER_LEVEL + 4)
					{
						return _cache.Reeds;
					}
					return _cache.GrassTuft;
				}
			}

			//And let's make some clouds
			if (z >= CLOUD_LEVEL && z < CLOUD_LEVEL_TOP && _clouds.GetValue(x,y,z) < 0.3f)
			{
				return _cache.Cloud;
			}

			//Once all of the above qualifications happen, the only thing left we can be is Air, which is "Empty"
			return _cache.Empty;
		}

	}
}

