using System;
using pluginbase.Objects.World;
using pluginbase.Objects.World.Blocks;
using essentials.Blocks.Earth;
using essentials.Blocks.Liquid;
using essentials.Blocks.Geological;
using essentials.Blocks.Gas;
using essentials.blocks.vegetation.Brush;
using essentials.blocks.vegetation.Underwater;

namespace builder.Worlds
{
	public class BlockCache
	{
		[BlockCache(typeof(Air))]
		public readonly IBlock Empty;


		[BlockCache(typeof(Grass))]
		public readonly IBlock Grass;

		[BlockCache(typeof(Rock))]
		public readonly IBlock Rock;

		[BlockCache(typeof(RockyGrass))]
		public readonly IBlock RockyGrass;

		[BlockCache(typeof(Dirt))]
		public readonly IBlock Dirt;

		[BlockCache(typeof(BeachSand))]
		public readonly IBlock BeachSand;

		[BlockCache(typeof(Sand))]
		public readonly IBlock Sand;

		[BlockCache(typeof(Water))]
		public readonly IBlock Water;



        [BlockCache(typeof(Fern))]
		public readonly IBlock Fern;

		[BlockCache(typeof(GrassBlades))]
		public readonly IBlock GrassTuft;

		[BlockCache(typeof(Reeds))]
		public readonly IBlock Reeds;

		[BlockCache(typeof(Bush))]
		public readonly IBlock Bush;

		[BlockCache(typeof(LargeBush))]
		public readonly IBlock BushLarge;

		[BlockCache(typeof(Boulder))]
		public readonly IBlock Boulder;

        [BlockCache(typeof(EelGrass))]
		public readonly IBlock EelGrass;

		[BlockCache(typeof(Vine))]
		public readonly IBlock Vine;

		[BlockCache(typeof(VinePair))]
		public readonly IBlock VinePair;


		[BlockCache(typeof(Mushroom))]
		public readonly IBlock Mushroom;

		[BlockCache(typeof(MushroomGroup))]
		public readonly IBlock MushroomGroup;

		[BlockCache(typeof(GlowingMushroom))]
		public readonly IBlock MushroomGlowing;

		[BlockCache(typeof(Cloud))]
		public readonly IBlock Cloud;

		[BlockCache(typeof(GasCloud))]
		public readonly IBlock Gas;

		[BlockCache(typeof(Lava))]
		public readonly IBlock Lava;

		[BlockCache(typeof(Stalagmite))]
		public readonly IBlock Stalagmite;

		[BlockCache(typeof(StalagmiteGroup))]
		public readonly IBlock StalagmiteGroup;

		[BlockCache(typeof(Stalactite))]
		public readonly IBlock Stalactite;

		[BlockCache(typeof(StalactiteLarge))]
		public readonly IBlock StalactiteLarge;

		[BlockCache(typeof(StalactiteGroup))]
		public readonly IBlock StalactiteGroup;
	}
}

