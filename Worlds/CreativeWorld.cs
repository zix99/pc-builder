using System;
using pluginbase.Attributes;
using pluginbase.Objects.World;
using pluginbase.Objects.World.Blocks;
using essentials.Blocks.Structural;
using pluginbase.Objects.World.Generators;
using builder.Blocks;

namespace builder.Worlds
{
    [WorldGenerator("CreativeFlat", Author = "Pontoon City", Description = "Flat world for building", IconPath = "icons/build.png")]
    public class CreativeWorld : WorldGeneratorBase
    {
        private readonly IBlock _base;
        private readonly IBlock _gridSmall;
        private readonly IBlock _gridLarge;
        private readonly IBlock _air;

        public CreativeWorld (int seed, IBlockLookup lookup)
        {
            _base = lookup.GetBlock<GrayBlock> ();
            _gridLarge = lookup.GetBlock<OrangeBlock> ();
            _gridSmall = lookup.GetBlock<LightGrayBlock> ();
            _air = lookup.EmptyBlock;
        }


        #region implemented abstract members of GeneratorBase
        public override IBlock GetBlock (int x, int y, int z)
        {
            if (z < 0)
            {
                if (z == -1) {
                    if (x % 32 == 0 || y % 32 == 0)
                        return _gridLarge;
                    if (x % 16 == 0 || y % 16 == 0)
                        return _gridSmall;
                }
                return _base;
            }
            return _air;
        }
        #endregion
    }
}

