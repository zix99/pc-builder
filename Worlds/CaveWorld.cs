using System;
using pluginbase.Objects.World.Generators;
using pluginbase.Attributes;
using pluginbase.Objects.World.Blocks;
using pluginbase.Objects.World;
using pluginbase.Helpers.Computative.Algorithms;
using pluginbase.Helpers.Computative.Random;

namespace builder.Worlds
{
	[WorldGenerator("Caves", Author = "Pontoon City", Description="Underground world", IconPath="icons/caves.png")]
	public class CaveWorld : WorldGeneratorBase
	{
		private readonly BlockCache _cache;

		private readonly SmoothPerlin _cave1, _cave2;
		private readonly SmoothPerlin _floor, _ceil;
		private readonly SmoothPerlin _wall;

		private Random3D _veg;

		public CaveWorld (int seed, IBlockLookup blockLookup)
		{
			_cache = blockLookup.PopulateCache<BlockCache>();

			_cave1 = new SmoothPerlin(seed+0, 128, 4);
			_cave2 = new SmoothPerlin(seed+2, 128, 4);

			_floor = new SmoothPerlin(seed+3, 128, 3);
			_ceil = new SmoothPerlin(seed+4, 64, 2);
			_wall = new SmoothPerlin(seed+5, 64, 2);

			_veg = new Random3D(seed);
		}

		public override IBlock GetBlock (int x, int y, int z)
		{
			float caveSum = Math.Abs(
					Math.Abs(_cave1.GetValue(x,y) - _cave2.GetValue(x,y))
				) + _wall.GetValue(x,y,z)*0.01f;

			int floor = _floor.GetValue(x,y,0,32);
			int ceilHeight = _ceil.GetValue(x,y,3,15);


			/*if ( z >= floor/2 && z <= floor+ceilHeight*2
			    && _lavaRooms.GetValue(x,y,z) < 0.35f)
			{
				return _cache.Empty;
			}*/


			if (caveSum < 0.03f && z >= floor && z <= floor+ceilHeight)
			{
				if (z < 10)
				{
					return _cache.Water;
				}

				if (z == floor)
				{
					switch(_veg.GetValue(x,y,0,100))
					{
						case 0:
							return _cache.MushroomGlowing;
						case 1:
						case 2:
							return _cache.Mushroom;
						case 3:
							return _cache.MushroomGroup;
						case 4:
							return _cache.Bush;
						case 5:
							return _cache.Stalagmite;
						case 6:
							return _cache.StalagmiteGroup;
						case 7:
							return _cache.Fern;
					}
				}

				if (z == floor + ceilHeight)
				{
					switch(_veg.GetValue(x,y,0,100))
					{
						case 50:
							return _cache.Stalactite;
						case 51:
							return _cache.StalactiteLarge;
						case 52:
							return _cache.StalactiteGroup;
					}
				}

				if (_veg.GetValue(x,y,0,100) == 60)
				{
					return _cache.VinePair;
				}

				return _cache.Empty;
			}

			return _cache.Rock;
		}
	}
}

