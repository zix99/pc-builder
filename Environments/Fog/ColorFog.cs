using System;
using pluginbase.Objects.World.Environment.Fog;
using pluginbase.Helpers.Data;

namespace builder.Environments.Fog
{
	public class ColorFog : FogBase
	{
        private readonly Rgba _color;
        private readonly float _density;

        public ColorFog(Rgba color, float density = 0.02f)
        {
            _color = color;
            _density = density;
        }

		public override Rgba Color {
			get {
				return _color;
			}
		}

		public override float Density {
			get {
				return _density;
			}
		}
	}
}

