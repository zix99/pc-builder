using System;
using pluginbase.Attributes;
using pluginbase.Objects.World.Environment;
using pluginbase.Objects.World.Environment.Orbitals;
using OpenTK;
using builder.Environments.Fog;
using pluginbase.Helpers.Data;
using pluginbase.Objects.World.Environment.Atmosphere;

namespace builder.Environments
{
    [Environment("Creative", Author = "Pontoon City", Description = "Always-day environment", IconPath = "icons/build.png")]
    public class CreativeEnvironment : EnvironmentBase
    {
        private class LightSource : OrbitalBase
        {
            private static readonly Vector3 _direction = Vector3.Normalize(new Vector3(1f, 0.5f, 1f));
            public override Vector3 Direction {
                get {
                    return _direction;
                }
            }

            public override bool HasEmittance {
                get {
                    return true;
                }
            }

            public override Rgba EmittanceDiffuse {
                get {
                    return new Rgba (1f, 1f, 1f, 1f);
                }
            }

            public override Rgba EmittanceSpecular {
                get {
                    return new Rgba (0.7f, 0.7f, 0.7f, 1f);
                }
            }
        }

        private class Atmos : AtmosphereBase
        {
            public override Vector3 GetWind (double x, double y, double z)
            {
                return Vector3.Zero;
            }
        }

        public CreativeEnvironment ()
        {
        }

        public override IOrbital[] CreateOrbitals ()
        {
            return new IOrbital[] {
                new LightSource()
            };
        }

        public override IFog CreateFog ()
        {
            return new ColorFog(Rgba.Gray, 0.01f);
        }

        public override Rgba SkyColor {
            get {
                return Rgba.Gray;
            }
        }

        public override IAtmosphere CreateAtmosphere ()
        {
            return new Atmos();
        }
    }
}

