using System;
using pluginbase.Objects.World.Environment;
using pluginbase.Attributes;
using pluginbase.Helpers.Data;
using builder.Environments.Fog;

namespace builder.Environments
{
	[Environment("Black", Author = "Pontoon City", Description="Pitch black environment. Dark. Dreary", IconPath="icons/black.png")]
	public class BlackEnvironment : EnvironmentBase
	{

		public BlackEnvironment ()
		{
		}

        public override IFog CreateFog ()
        {
            return new ColorFog(Rgba.Zero);
        }

		public override Rgba SkyColor {
			get {
				return Rgba.Black;
			}
		}
	}
}

