using System;
using essentials.action.Entities.Actor;
using pluginbase.Objects.Syncable.Entity.Controllers;
using OpenTK;
using pluginbase.Helpers.Coords;
using pluginbase.Dependencies;
using pluginbase.Objects.Models;
using pluginbase.Objects.Syncable.Entity;
using pluginbase.Objects.Input;
using OpenTK.Input;
using pluginbase.Helpers.Computative;
using builder.Entities.Components;

namespace builder.Entities
{
    [Entity("FlyingPlayer")]
    public class FlyingPlayer : ActorBase, IPlayableEntity
    {
        [Dependency(Optional = true)]
        private readonly IModelFactory _modelFactory;

        [Dependency]
        private readonly IResourceResolver _resolver;

        [Dependency(Optional = true)]
        private readonly IInputManager _inputManager;

        private IBoundInput _inputBind;

        private BuildingClientComponent _buildingComponent;
        private PrefabComponent _prefabComponent;

        public FlyingPlayer ()
        {
            this.InjectDependencies ();
        }

        #region implemented abstract members of ActorBase

        protected override IModelInstance CreateModel ()
        {
            return _modelFactory.CreateModelInstance (_resolver.Resolve ("cursors/cursor.obj"));
        }

        #endregion

        #region IPlayableEntity implementation

        private static class KeyNames
        {
            public const string FORWARD = "Forward";
            public const string BACKWARD = "Backward";
            public const string LEFT = "Left";
            public const string RIGHT = "Right";
            public const string UP = "Jump";
            public const string DOWN = "Crouch";

            public const string FAST = "Sprint";
        }

        [EntityHook(EntityHook.Init, EntityScope.Player)]
        private void InitPlayer ()
        {
            var binding = _inputBind = _inputManager.GetBinding (typeof(FlyingPlayer));

            binding.Listen (KeyNames.FORWARD, Key.W);
            binding.Listen (KeyNames.BACKWARD, Key.S);
            binding.Listen (KeyNames.LEFT, Key.A);
            binding.Listen (KeyNames.RIGHT, Key.D);
            binding.Listen (KeyNames.UP, Key.Q);
            binding.Listen (KeyNames.DOWN, Key.Z);
            binding.Listen (KeyNames.FAST, Key.ShiftLeft);

            binding.BindInputMethods (this);
            binding.StartListening ();

            _buildingComponent = new BuildingClientComponent ();
            _prefabComponent = new PrefabComponent ();
        }

        [EntityHook(EntityHook.Shutdown, EntityScope.Player)]
        private void ShutdownPlayer ()
        {
            _inputBind.Dispose();
        }

        [EntityHook(EntityHook.Simulate, EntityScope.Player)]
        private void SimulatePlayer (IFrameTimeState timeState)
        {
            const double DEFAULT_SPEED = 4.0;
            const double MAX_VERTICAL_ANGLE = MExt.Pi / 2.0 - 0.005;
            double speed = DEFAULT_SPEED * (_inputBind.IsDown (KeyNames.FAST) ? 5.0 : 1.0);

            var mouseDelta = _inputBind.GetMouseDelta();
            this.Rotation = new Rotation2d(
                MExt.Clamp(this.Rotation.X - mouseDelta.Y * 0.005, -MAX_VERTICAL_ANGLE, MAX_VERTICAL_ANGLE),
                this.Rotation.Z + mouseDelta.X * 0.005
                );

            if (_inputBind.IsDown(KeyNames.FORWARD))
            {
                this.Position += this.Rotation.Vector * speed * timeState.Delta;
            }
            if (_inputBind.IsDown(KeyNames.BACKWARD))
            {
                this.Position -= this.Rotation.Vector * speed * timeState.Delta;
            }
            if (_inputBind.IsDown(KeyNames.LEFT))
            {
                this.Position += VExt.RotationVector (this.Rotation.Z - MathHelper.PiOver2, speed * 0.75f * timeState.Delta);
            }
            if (_inputBind.IsDown(KeyNames.RIGHT))
            {
                this.Position += VExt.RotationVector (this.Rotation.Z + MathHelper.PiOver2, speed * 0.75f * timeState.Delta);
            }
            if (_inputBind.IsDown(KeyNames.UP))
            {
                this.Position += new Vector3d (0.0, 0.0, speed * timeState.Delta);
            }
            if (_inputBind.IsDown(KeyNames.DOWN))
            {
                this.Position -= new Vector3d (0.0, 0.0, speed * timeState.Delta);
            }

            _buildingComponent.Simulate (this.Position + this.CameraOffset, this.Rotation, 10.0);
        }

        public Vector3d CameraOffset {
            get {
                return Vector3d.Zero;
            }
        }

        public Rotation2d CameraRotationOffset {
            get {
                return Rotation2d.Zero;
            }
        }

        public double? FieldOfViewOverride
        {
            get{return null;}
        }

        [Input(Key.V)]
        private void PrefabIntelliDetect()
        {
            if (this._buildingComponent.CursorNextPos.HasValue)
            {
                _prefabComponent.IntelliDetect (this._buildingComponent.CursorNextPos.Value);
                _prefabComponent.ShowCursor = true;
            }
        }

        [Input(Key.BracketLeft)]
        private void CopySelect()
        {
            if (this._buildingComponent.CursorNextPos.HasValue)
            {
                _prefabComponent.IntelliDetect (this._buildingComponent.CursorNextPos.Value);
                _prefabComponent.ShowCursor = true;
                _prefabComponent.Copy ();
            }
        }

        [Input(Key.BracketRight)]
        private void PasteSelect()
        {
            if (this._buildingComponent.CursorNextPos.HasValue)
            {
                _prefabComponent.Paste (this._buildingComponent.CursorNextPos.Value);
                _prefabComponent.ShowCursor = false;
            }
        }

        #endregion
    }
}

