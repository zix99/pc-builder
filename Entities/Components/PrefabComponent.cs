using System;
using System.Linq;
using pluginbase.Objects.WorldObjects;
using pluginbase.Dependencies;
using pluginbase.Objects.UI;
using pluginbase.Helpers.Coords;
using OpenTK;
using pluginbase.Objects.UI.Glue;
using essentials.Prefabs;
using pluginbase.Objects.Libs.Rpc;
using builder.Games;
using pluginbase.Objects.Tasks;
using pluginbase.Objects.Input;
using OpenTK.Input;
using System.Collections.Generic;
using pluginbase.Objects.World;
using pluginbase.Objects.World.Blocks;
using builder.Blocks;
using pluginbase.Objects.Models;
using pluginbase.Objects.Cache;

namespace builder.Entities.Components
{
	public class PrefabComponent
	{
		private readonly IWorldObject _selectorObject;
		private readonly IWorldObject _centerObject;
        private readonly IWorldObject _labelObject;

        private class PrefabLabel
        {
            public Vector3i Pos;
            public string Label;
            public IWorldObject Obj;
        }

        private readonly List<PrefabLabel> _labels = new List<PrefabLabel> ();
        private readonly IReadonlyModel _labelModel;

		[Dependency]
		private readonly IWorldObjectManager _objectManager;

        [Dependency]
        private readonly IModelFactory _modelFactory;

		[Dependency]
		private readonly IResourceResolver _resolver;

		[Dependency]
		private readonly IBuiScreen _screen;

		[Dependency]
		private readonly ICacheProvider _cache;

		[Dependency]
		private readonly IRpcManager _rpcManager;

		[Dependency]
		private readonly ITaskScheduler _taskScheduler;

        [Dependency]
        private readonly IInputManager _inputManager;

        [Dependency]
        private readonly IWorldReadonly _world;

		private readonly IRpcClass _buildRpc;
		private readonly IBuiControl _prefabController;

        private IBoundInput _boundInput;

        public Vector3i Position
        {
            get{ return new Vector3i (this.X, this.Y, this.Z); }
            set
            {
                this.X = value.X;
                this.Y = value.Y;
                this.Z = value.Z;
            }
        }

        public Size3i Size
        {
            get{return new Size3i (this.Width, this.Length, this.Height);}
            set
            {
                this.Width = value.Width;
                this.Length = value.Length;
                this.Height = value.Height;
            }
        }

        public Vector3i Center
        {
            get{return new Vector3i (this.CenterX, this.CenterY, this.CenterZ);}
            set
            {
                this.CenterX = value.X;
                this.CenterY = value.Y;
                this.CenterZ = value.Z;
            }
        }

        #region Model

        [Glue("Status")]
        protected string Status;

        [Glue("X")]
        public int X = 0;

        [Glue("Y")]
        public int Y = 0;

        [Glue("Z")]
        public int Z = 0;

        [Glue("Width")]
        public int Width = 1;

        [Glue("Length")]
        public int Length;

        [Glue("Height")]
        public int Height;

        [Glue("CenterX")]
        public int CenterX = 0;

        [Glue("CenterY")]
        public int CenterY = 0;

        [Glue("CenterZ")]
        public int CenterZ = 0;

        [Glue("ShowCursor")]
        public bool ShowCursor = false;

        [Glue("Name")]
        public string Name = string.Empty;

        [Glue("Visible")]
        public bool Visible = false;

        [Glue("TotalVolume", Writeable = false)]
        public int TotalVolume = 0;

        [Glue("TotalBlocks", Writeable = false)]
        public int TotalBlocks = 0;

        [Glue("LabelX")]
        public int LabelX = 0;

        [Glue("LabelY")]
        public int LabelY = 0;

        [Glue("LabelZ")]
        public int LabelZ = 0;

        #endregion

		public PrefabComponent()
		{
			this.InjectDependencies();
			_selectorObject = _objectManager.Create(_resolver.Resolve("components/prefab/selector.obj"));
			_centerObject = _objectManager.Create(_resolver.Resolve("components/prefab/center.obj"));
            _labelModel = _modelFactory.LoadModel(_resolver.Resolve("components/prefab/meta.obj"));

            _labelObject = _objectManager.Create (_resolver.Resolve ("components/prefab/selector.obj"));
            _labelObject.ModelInstance.Transform = Matrix4.CreateScale (1.1f, 1.1f, 1.1f) * Matrix4.CreateTranslation (-0.05f, -0.05f, -0.05f);

			_buildRpc = _rpcManager.GetClass<BuildingSubsystem>();

			_prefabController = _screen.AddControl(_resolver.Resolve("components/prefab/ui.bml"));
            _prefabController.AddCallback (this);
			var glue = _prefabController.GlueModel(this);
			glue.PropertyUpdate += x => this.Refresh();

            _boundInput = _inputManager.BindInputMethods (this);
            _boundInput.StartListening ();
		}

		public void Refresh()
		{
            _selectorObject.Position = new Vector3d (this.X, this.Y, this.Z);
            _selectorObject.Scale = new Vector3d (this.Width, this.Length, this.Height);
            _selectorObject.Visible = this.ShowCursor;

            _centerObject.Position = new Vector3d (this.X + this.CenterX, this.Y + this.CenterY, this.Z + this.CenterZ);
            _centerObject.Visible = this.ShowCursor;

            this.TotalVolume = this.Width * this.Length * this.Height;
            var prefab = new PrefabWriterInstance(this.Position + this.Center, this.Size, this.Center);
            prefab.Store ();
            this.TotalBlocks = prefab.CountBlocks ();

            _labelObject.Position = this.Position + new Vector3i (this.LabelX, this.LabelY, this.LabelZ);
            _labelObject.Visible = this.ShowCursor;

            //labels object
            foreach(var label in _labels)
            {
                label.Obj.Position = label.Pos + this.Position;
                label.Obj.Visible = this.ShowCursor;
            }
		}

		private string GetFilename(string slug)
		{
			if (slug == null)
				return GetFilename("undefined");
			return slug.Trim() + ".pfb";
		}

        [Glue("Save")]
		protected void SaveSelection()
		{
			if (!string.IsNullOrWhiteSpace(this.Name))
			{
                var instance = new PrefabWriterInstance(this.Position + this.Center, this.Size, this.Center);
				instance.Store();

                foreach(var mp in _labels)
                {
                    instance.Prefab.SetLabel (mp.Pos, mp.Label);
                }

				using(var file = _cache.OpenWrite(GetFilename(this.Name)))
				{
					instance.Prefab.Save(file);
				}
                this.Status = "Saved";
			}
            else
            {
                this.Status = "Need name";
            }
		}

        [Glue("Load")]
		protected void LoadByName()
		{
            string filename = GetFilename(this.Name);
			if (_cache.Exists(filename))
			{
				using (var stream = _cache.OpenRead(filename))
				{
					var prefab = Prefab.Load(stream);

                    //Set labels
                    _labels.Clear ();
                    foreach(var label in prefab.Labels)
                    {
                        var l = new PrefabLabel {
                            Label = label.Value,
                            Pos = label.Key,
                            Obj = _objectManager.Create(_labelModel)
                        };
                        l.Obj.Position = this.Position + l.Pos;
                        _labels.Add (l);
                    }
                    UpdateUILabels ();

                    //Construct
                    this.Status = "Building...";
                    var inst = prefab.CreateInstance(this.Position).CreateConstructor();
					_taskScheduler.ScheduleRecurringTask(() => {

						var block = inst.Pop();
						if (block.HasValue)
						{
							_buildRpc.Invoke("PlaceBlock", RpcTarget.Server, block.Value.WorldPos, block.Value.Block.Id);
							return TaskResult.Continue;
						}
                        this.Status = "Loaded";
						return TaskResult.Remove;
					}, TimeSpan.FromMilliseconds(10));

                    //Set up my point-data
                    this.Position -= prefab.Center;
                    this.Center = prefab.Center;
                    this.Size = prefab.Size;
				}
			}
            else
            {
                this.Status = "File not found";
            }
		}

        [Glue("Clear")]
        protected void ClearInstance()
        {
            this.X = this.Y = this.Z = 0;
            this.Width = this.Length = this.Height = 1;
            this.CenterX = this.CenterY = this.CenterZ = 0;
            this.Name = string.Empty;
            this._labels.Clear ();
            UpdateUILabels ();
        }

        [Glue("Zero-Center")]
        protected void ZeroCenter()
        {
            this.CenterX = this.CenterY = this.CenterZ = 0;
        }

        [Glue("Center-Center")]
        protected void CenterCenter()
        {
            this.CenterX = this.Width / 2;
            this.CenterY = this.Length / 2;
        }

        [Glue("One-Size")]
        protected void OneSize()
        {
            this.Size = Size3i.One;
        }

        [Input(Key.C)]
        private void OpenPrefab()
        {
            _prefabController.Invoke ("ToggleOpen");
        }

        [BuiCallback("AddLabel")]
        private void AddLabel(IBuiControl control, object[] args)
        {
            var pos = new Vector3i ((int)args [0], (int)args [1], (int)args [2]);

            if (!_labels.Any (x => x.Pos == pos))
            {
                var mp = new PrefabLabel {
                    Pos = pos,
                    Label = (string)args[3],
                    Obj = _objectManager.Create(_labelModel)
                };
                mp.Obj.Position = mp.Pos + this.Position;

                _labels.Add (mp);
                UpdateUILabels ();
            }
        }

        [BuiCallback("RemLabel")]
        private void RemoveLabel(IBuiControl control, object[] args)
        {
            var pos = new Vector3i ((int)args[0], (int)args[1], (int)args[2]);
            _labels.RemoveAll(mp => mp.Pos == pos);
            UpdateUILabels ();
        }

        private Vector3i _currSelected;
        [BuiCallback("SelectLabel")]
        private void SelectLabel(IBuiControl control, object[] args)
        {
            var pos = new Vector3i ((int)args [0], (int)args [1], (int)args [2]);
            SetLabelScale (_currSelected, 1.0f);
            _currSelected = pos;
            SetLabelScale (pos, 1.5f);
        }

        private void SetLabelScale(Vector3i labelPos, float scale)
        {
            var curr = _labels.FirstOrDefault (x => x.Pos == labelPos);
            if (curr != null)
            {
                curr.Obj.ModelInstance.Transform = Matrix4.CreateScale (scale) * Matrix4.CreateTranslation (new Vector3 ((scale - 1f) / -2f));
            }
        }

        private void UpdateUILabels()
        {
            _prefabController.Invoke ("UpdateLabels", _labels);
        }

        public void IntelliDetect(Vector3i start, Func<IBlock, bool> predicate)
        {
            var toSearch = new Queue<Vector3i> ();
            var found = new HashSet<Vector3i> ();
            toSearch.Enqueue (start);

            //Scan all blocks
            while(toSearch.Count > 0 && found.Count < 10000)
            {
                var pt = toSearch.Dequeue ();
                if (found.Add (pt))
                {
                    for (int i=0; i<Vector3i.AllDirections.Length; ++i)
                    {
                        var newPt = pt + Vector3i.AllDirections[i];
                        if (!found.Contains (newPt) && predicate (_world [newPt]))
                            toSearch.Enqueue (newPt);
                    }
                }
            }

            //Get maximums and minimums
            var max = found.Aggregate (Vector3i.ComponentMax);
            var min = found.Aggregate (Vector3i.ComponentMin);

            //Set bounds
            this.Position = min;
            this.Size = (Size3i)(max - min + Vector3i.One);
            this.CenterX = this.Width / 2;
            this.CenterY = this.Length / 2;
            this.CenterZ = 0;
        }

        public void IntelliDetect(Vector3i start)
        {
            this.IntelliDetect (start, block => block.Renderable && !(block is IVirtualBlock));
        }



        private IPrefab _clipboard;

        public void Copy()
        {
            var instance = new PrefabWriterInstance(this.Position + this.Center, this.Size, this.Center);
            instance.Store();
            _clipboard = instance.Prefab;
        }

        public void Paste(Vector3i pos)
        {
            if (_clipboard != null)
            {
                var writer = _clipboard.CreateWriterInstance (pos);
                var constructor = writer.CreateConstructor ();

                _taskScheduler.ScheduleRecurringTask(() => {

                    var block = constructor.Pop();
                    if (block.HasValue)
                    {
                        _buildRpc.Invoke("PlaceBlock", RpcTarget.Server, block.Value.WorldPos, block.Value.Block.Id);
                        return TaskResult.Continue;
                    }
                    this.Status = "Loaded";
                    return TaskResult.Remove;
                }, TimeSpan.FromMilliseconds(10));
            }
        }
	}
}

