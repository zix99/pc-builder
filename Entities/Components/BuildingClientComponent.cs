using System;
using System.Linq;
using pluginbase.Objects.UI;
using pluginbase.Dependencies;
using pluginbase.Objects.Input;
using pluginbase.Objects.WorldObjects;
using OpenTK.Input;
using pluginbase.Objects.World.Blocks;
using pluginbase.Objects.World;
using OpenTK;
using pluginbase.Helpers.Coords;
using pluginbase.Helpers.Geometry;
using essentials.Blocks.Structural;
using pluginbase.Objects.Libs.Rpc;
using builder.Games;
using pluginbase.Objects.Audio;

namespace builder.Entities.Components
{
    public class BuildingClientComponent
    {
        [Dependency]
        private readonly IBuiScreen _bui;

        [Dependency]
        private readonly IInputManager _inputManager;

        [Dependency]
        private readonly IWorldObjectManager _objectManager;

        [Dependency]
        private readonly IResourceResolver _resolver;

        [Dependency]
        private readonly IBlockLookup _blockLookup;

        [Dependency]
        private readonly IWorld _world;

        [Dependency]
        private readonly IRpcManager _rpcManager;

        [Dependency]
        private readonly IBlockAudioManager _audioBlocks;

        private readonly IRpcClass _rpc;

        private readonly IBuiControl _hud;
        private readonly IWorldObject _cursor;
        private readonly IBoundInput _input;

        public BuildingClientComponent ()
        {
            this.InjectDependencies ();

            _rpc = _rpcManager.GetClass<BuildingSubsystem> ();

            _hud = _bui.AddControl (_resolver.Resolve ("ui/hud.bml"));

            _input = _inputManager.GetBinding (typeof(BuildingClientComponent));

            //Bind keys to hud
            for (int i=0; i<10; ++i)
            {
                int currIdx = (i+9) % 10;
                _input.Bind(string.Format("Select-{0}", i), Key.Number0 + i, () => _hud.Invoke("select", currIdx));
            }

            _input.Bind("Inventory", Key.E, () => _hud.Invoke("toggleBlockList"));

            _input.BindInputMethods (this);
            _input.StartListening ();

            //Add all block types
            foreach(IBlock block in this._blockLookup.Blocks.OrderBy(x => x.Name))
            {
                _hud.Invoke("addBlockType", block.TextureResource, block.Name, block.Id);
            }

            //Load the cursor
            _cursor = this._objectManager.Create(this._resolver.Resolve("cursors/cursor.obj"));
        }

        private Vector3i? _cursorPos;
        private Vector3i? _nextPos;

        public Vector3i? CursorPos
        {
            get{ return _cursorPos;}
        }

        public Vector3i? CursorNextPos
        {
            get{return _nextPos;}
        }

        public void Simulate(Vector3d pos, Rotation2d rot, double viewDistance)
        {
            var toReplace = this._world.BeforeFirst(new Segment(pos, rot.Vector, viewDistance), x => x.Block.Solid);
            if (toReplace != null)
            {
                _cursor.Visible = true;
                _cursor.Position = toReplace.Coordinate.CenterDbl;
                _cursorPos = toReplace.Coordinate;

                var nextPos = this._world.First (new Segment (pos, rot.Vector, viewDistance), x => x.Block.Solid);
                if (nextPos != null)
                    _nextPos = nextPos.Coordinate;
                else
                    _nextPos = null;
            }
            else
            {
                _cursorPos = null;
                _cursor.Visible = false;
            }
        }

        //Invoked on the client by a keybinding in InitPlayer
        [Input(MouseButton.Left)]
        private void HandlePlaceClick()
        {
            if (_cursorPos.HasValue)
            {
                ushort blockTypeId = _hud.GetValue<ushort>("SelectedBlock", this._blockLookup.GetBlock<RedBrick>().Id);
                _rpc.Invoke ("Place", RpcTarget.Server, _cursorPos.Value, blockTypeId);
                this._audioBlocks[blockTypeId].Place.Play(_cursorPos.Value);
            }
        }

        [Input(MouseButton.Right)]
        private void HandleRemoveClick()
        {
            if (_nextPos.HasValue)
            {
                _rpc.Invoke ("Destroy", RpcTarget.Server, _nextPos.Value);
                var block = _world [_nextPos.Value].Block;
                this._audioBlocks[block].Break.Play(_nextPos.Value);
            }
        }

        [Input(MouseButton.Middle)]
        private void HandleSelectClick()
        {
            if (_nextPos.HasValue)
            {
                _hud.SetValue<ushort> ("SelectedBlock", this._world [_nextPos.Value].Block.Id);
            }
        }

        [Input(Key.F)]
        private void HandleInteraction()
        {
            if (_nextPos.HasValue && _world[_nextPos.Value].Block is IBlockInteractable)
            {
                _rpc.Invoke ("Interact", RpcTarget.Server, _nextPos.Value);
            }
            else if (_cursorPos.HasValue && _world[_cursorPos.Value].Block is IBlockInteractable)
            {
                _rpc.Invoke ("Interact", RpcTarget.Server, _cursorPos.Value);
            }
        }
    }
}

