using essentials.action.Entities.Actor;
using pluginbase.Objects.Syncable.Entity;
using pluginbase.Dependencies;
using pluginbase.Objects.Models;

namespace builder.Entities
{
	[Entity("BuilderPlayer")]
	public partial class Player : PlayableBase
	{
		[Dependency]
		protected IModelFactory ModelFactory;

		[Dependency]
		protected IResourceResolver Resources;


		public Player ()
		{
			//This call tries to populate all properties with the [Dependency] attribute applied to them
			this.InjectDependencies();
		}

		protected override IModelInstance CreateModel ()
		{
			return this.ModelFactory.CreateModelInstance( this.EssentialsActionResources.Resolve("essentials", "models/peasant1.xml") );
		}
	}
}

