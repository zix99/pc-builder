using pluginbase.Dependencies;
using builder.Entities.Components;
using pluginbase.Objects.Syncable.Entity.Controllers;

namespace builder.Entities
{
	public partial class Player
	{
		private PrefabComponent _prefabUi;
        private BuildingClientComponent _building;
		
		/// <summary>
		/// Inits the player.  This is called when the current entity is the player on the client
		/// Please note that this has no relation to the name of the class
		/// </summary>
        [EntityHook(EntityHook.Init, EntityScope.Player)]
        private void InitPlayer ()
		{
			_prefabUi = new PrefabComponent();
            _building = new BuildingClientComponent ();
		}

        [EntityHook(EntityHook.Simulate, EntityScope.Player)]
        private void SimulatePlayer (IFrameTimeState timeState)
		{
            _building.Simulate (this.Position + this.CameraOffset, this.Rotation, 4.0);
		}
	}
}

