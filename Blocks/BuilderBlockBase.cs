using System;
using pluginbase.Objects.World.Blocks;
using pluginbase.Dependencies;

namespace builder.Blocks
{
    public abstract class BuilderBlockBase : BlockBase, IVirtualBlock
    {
        [Dependency]
        protected readonly IResourceResolver BuilderResources;

        protected BuilderBlockBase()
        {
            this.InjectDependencies();
        }

        public override BlockVertexMode VertexMode {
            get {
                return BlockVertexMode.Cubic;
            }
        }

        public override MaterialType MaterialType {
            get {
                return MaterialType.NonMaterial;
            }
        }
    }
}

