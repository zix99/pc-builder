using System;
using pluginbase.Attributes;

namespace builder.Blocks
{
    [Block("LightGray")]
    public class LightGrayBlock : BuilderBlockBase
    {
        public override string Name {
            get {
                return "Light Gray Block";
            }
        }

        public override string TextureResource {
            get {
                return this.BuilderResources.Resolve ("blocks/lightgray.png");
            }
        }
    }
}

