using System;

namespace builder.Blocks
{
    /// <summary>
    /// Label to designate blocks as part of the virtual world
    /// Largely for prefab detection
    /// </summary>
    internal interface IVirtualBlock
    {
    }
}

