using System;
using pluginbase.Attributes;

namespace builder.Blocks
{
    [Block("Gray")]
    public class GrayBlock : BuilderBlockBase
    {
        public override string Name {
            get {
                return "Gray Block";
            }
        }

        public override string TextureResource {
            get {
                return this.BuilderResources.Resolve ("blocks/gray.png");
            }
        }
    }
}

