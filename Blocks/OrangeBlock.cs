using System;
using pluginbase.Attributes;

namespace builder.Blocks
{
    [Block("Orange")]
    public class OrangeBlock : BuilderBlockBase
    {
        public override string Name {
            get {
                return "Orange block";
            }
        }

        public override string TextureResource {
            get {
                return this.BuilderResources.Resolve("blocks/orange.png");
            }
        }
    }
}

