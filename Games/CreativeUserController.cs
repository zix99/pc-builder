using System;
using pluginbase.Objects.Game;
using pluginbase.Dependencies;
using pluginbase.Objects.Syncable.Entity.Controllers;
using builder.Entities;
using pluginbase.Objects.World;

namespace builder.Games
{
    public class CreativeUserController : UserControllerBase
    {
        [Dependency]
        private readonly IWorld _world;

        public CreativeUserController (IEntityManager entityManager, IUserManager userManager)
            :base(entityManager, userManager)
        {
            this.InjectDependencies ();
        }

        #region implemented abstract members of UserControllerBase

        public override IPlayableEntity CreateEntityFor (IUser user)
        {
            var entity = this.EntityManager.Load (user.Username) as FlyingPlayer;

            if (entity == null)
            {
                //New player
                entity = new FlyingPlayer ();
                var pt = _world.Generator.GetOpenPointNear (0, 0, 0);
                entity.SetPosition (pt.X, pt.Y, pt.Z);
            }

            entity.SetName (user.Username);

            return entity;
        }

        public override void UnloadEntityFor (IUser user, IPlayableEntity entity)
        {
            this.EntityManager.Persist (user.Username, entity);
            base.UnloadEntityFor (user, entity);
        }

        #endregion
    }
}

