using System;
using pluginbase.Objects.Game;
using pluginbase.Attributes;
using essentials.Subsystems.Chat;
using essentials.Subsystems.Scoreboard;
using pluginbase.Dependencies;
using essentials.action.Entities.Basic;

namespace builder.Games
{
	[GameContext("Builder", Author="Pontoon City", Description="Creative building with unlimited resources", IconPath="icons/build.png")]
	public class BuilderGame : PluginGameContextBase
	{
		public BuilderGame ()
		{
		}

		public override IClientSubsystem[] CreateClientSubsystems ()
		{
			return new IClientSubsystem[] {
				new ChatClient(),
				new ScoreboardClient()
			};
		}

		public override IServerSubsystem[] CreateServerSubsystems ()
		{
			return new IServerSubsystem[]{
				new ChatServer(),
				new ScoreboardServer(),
				new BuildingSubsystem()
			};
		}


		public override IUserController CreateUserController (IEntityManager entityManager, IUserManager userManager)
		{
			return new UserController(entityManager, userManager);
		}

	}
}

