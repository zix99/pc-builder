using System;
using pluginbase.Objects.Game;
using pluginbase.Objects.Syncable.Entity.Controllers;
using pluginbase.Dependencies;
using essentials.action.Entities.Basic;
using pluginbase.Objects.World;
using builder.Entities;

namespace builder.Games
{
	public class UserController : UserControllerBase
	{
		[Dependency]
		protected IWorld World{get; private set;}

		public UserController (IEntityManager entityManager, IUserManager userManager)
			:base(entityManager, userManager)
		{
			this.InjectDependencies();
		}


		public override IPlayableEntity CreateEntityFor (IUser user)
		{
			var loadedEntity = this.EntityManager.Load(user.Username);
			Player entity = loadedEntity as Player;

			if (entity == null)
			{
				entity = new Player();
				var pt = World.Generator.GetOpenPointNear(0,0,0,2,32);
				entity.SetPosition(pt.X, pt.Y, pt.Z);
			}

			entity.SetName(user.Username);

			return entity;
		}

		public override void UnloadEntityFor (IUser user, IPlayableEntity entity)
		{
			this.EntityManager.Persist(user.Username, entity);

			base.UnloadEntityFor (user, entity);
		}

	}
}

