using System;
using pluginbase.Objects.Game;
using pluginbase.Dependencies;
using pluginbase.Objects.Libs.Rpc;
using pluginbase.Helpers.Coords;
using pluginbase.Objects.World;
using pluginbase.Objects.World.Blocks;

namespace builder.Games
{
	public class BuildingSubsystem : IServerSubsystem
	{

		[Dependency]
		private readonly IRpcManager _rpcManager;

		[Dependency]
		private readonly IWorld _world;

		public BuildingSubsystem()
		{
			this.InjectDependencies();
		}

		[Remote("PlaceBlock")]
		public bool PlaceBlock(Vector3i pos, ushort blockType)
		{
			_world[pos].Block = _world.BlockLookup[blockType];
			return true;
		}

        [Remote("Place")]
        public void Place(Vector3i pos, ushort blockType)
        {
            _world [pos].Block = _world.BlockLookup [blockType];
        }

        [Remote("Destroy")]
        public void Destroy(Vector3i pos)
        {
            _world [pos].Block = _world.BlockLookup.EmptyBlock;
        }

        [Remote("Interact")]
        public void Interact(Vector3i pos)
        {
            var block = _world.GetIBlock(pos);

            var interact = block as IBlockInteractable;
            if (interact != null)
            {
                interact.Interact (_world, pos);
            }
        }

		#region ISubsystem implementation
		public void OnStart ()
		{
			_rpcManager.RegisterClass(this);
		}

		public void OnStop ()
		{

		}

		public void Simulate (pluginbase.Dependencies.IFrameTimeState frameTime)
		{

		}
		#endregion



	}
}

