using System;
using pluginbase.Attributes;
using pluginbase.Objects.Game;
using essentials.Subsystems.Chat;
using essentials.Subsystems.Scoreboard;

namespace builder.Games
{
    [GameContext("Creative", Author="Pontoon City", Description = "Creative mode where flying is possible", IconPath = "icons/build.png")]
    public class CreativeGame : PluginGameContextBase
    {
        public CreativeGame ()
        {
        }

        #region implemented abstract members of PluginGameContextBase

        public override IUserController CreateUserController (pluginbase.Dependencies.IEntityManager entityManager, pluginbase.Dependencies.IUserManager userManager)
        {
            return new CreativeUserController (entityManager, userManager);
        }

        public override IServerSubsystem[] CreateServerSubsystems ()
        {
            return new IServerSubsystem[] {
                new ChatServer(),
                new BuildingSubsystem(),
                new ScoreboardServer()
            };
        }

        public override IClientSubsystem[] CreateClientSubsystems ()
        {
            return new IClientSubsystem[] {
                new ChatClient(),
                new ScoreboardClient()
            };
        }

        #endregion
    }
}

