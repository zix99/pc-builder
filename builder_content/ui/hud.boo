import OpenTK.Input

class Controller:
	final UI as IBuiInterface = null

	_selected = 0
	_selectedImage = null
	_hotbarItems = []

	_items = []
	_itemLookup = {}

	_hotbarAddIndex = 0

	def constructor(ui):
		UI = ui

	def __init():
		for i in range(10):
			w = UI.LoadControl("hotitem.bml")
			w["id"].Value = "0"
			w["img"].Image = UI.ResolvePath("../icons/slot.png")
			w.View.X = i * w.View.Width
			_hotbarItems.Add(w)
			UI["hotbar"].AddControl(w)

		_selectedImage = UI.Create("picturebox")
		_selectedImage.Width = 40
		_selectedImage.Height = 40
		_selectedImage.Image = UI.ResolvePath("../icons/selected.png")
		UI["hotbar"].AddControl(_selectedImage)

	def select(idx):
		_selected = idx
		_selectedImage.X = idx * _selectedImage.Width

		widget = _hotbarItems[idx] as IBuiInterface
		UI["selectname"].Text = widget["img"].Tooltip

	def toggleBlockList():
		UI["blockchooser"].Visible = not UI["blockchooser"].Visible
		UI.Call("enable_cursor", UI["blockchooser"].Visible)
		UI["filter"].Focus()

	def addBlockType(imgFile, name, bid):
		#Create a block and populate
		item = UI.LoadControl("block.bml")
		item["img"].Image = imgFile
		item["name"].Text = name
		item["id"].Value = bid.ToString()
		_items.Push(item)
		_itemLookup[bid] = item
		UI["blocklist"].AddItem(item)

		if _hotbarAddIndex < 10:
			selectItem(_hotbarAddIndex, item)
			_hotbarAddIndex += 1

	def blocklist_ItemSelectedEvent(widget):
		intf = widget as IBuiInterface
		selectItem(_selected, intf)

	def filter_KeyEvent(widget, key as Key, down as bool):
		if down:
			if key == Key.Escape:
				UI["filter"].LoseFocus()
				toggleBlockList()
				_clearFilter()
			elif key == Key.Enter:
				selectItem(_selected, UI["blocklist"].SelectedItem as IBuiInterface)
				UI["filter"].LoseFocus()
				toggleBlockList()
				_clearFilter()
			elif key == Key.Up:
				UI["blocklist"].SelectedIndex = UI["blocklist"].SelectedIndex - 1
			elif key == Key.Down:
				UI["blocklist"].SelectedIndex = UI["blocklist"].SelectedIndex + 1
			else:
				_updateFilter()

	def _clearFilter():
		UI["filter"].Text = ""
		_updateFilter()

	def _updateFilter():
		list = UI["blocklist"]
		list.Clear()
		list.ScrollValue = 0
		filter = "(?i)" + UI["filter"].Text
		for item in _items:
			ctrl = item as IBuiInterface
			if ctrl["name"].Text =~ filter:
				list.AddItem(ctrl)

	def selectItem(slotIdx as int, itemWidget as IBuiInterface):
		hotbarItem = _hotbarItems[slotIdx] as IBuiInterface
		hotbarItem["img"].Image = itemWidget["img"].Image
		hotbarItem["img"].Tooltip = itemWidget["name"].Text
		hotbarItem["id"].Value = itemWidget["id"].Value
		UI["selectname"].Text = itemWidget["name"].Text

	SelectedBlock as ushort:
		get:
			item = _hotbarItems[_selected] as IBuiInterface
			return ushort.Parse(item["id"].Value)
		set:
			selectItem(_selected, _itemLookup[value])


