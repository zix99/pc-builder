#include "GlueStick.boo"

class Controller(GlueStick):
	UI as IBuiInterface = null

	def constructor(ui):
		super(ui)
		UI = ui
		print 123123

	_focused = false
	def ToggleOpen():
		if not _focused:
			UI.Call("enable_cursor", true)
			UI["prefab"].Visible = true
			_focused = true
		else:
			UI.Call("enable_cursor", false)
			UI["prefab"].Visible = false
			_focused = false

	def Unfocus_ButtonClickEvent(o):
		UI.Call("enable_cursor", false)
		_focused = false

	def AddLabel_ButtonClickEvent(o):
		x = UI["LabelX"].Value
		y = UI["LabelY"].Value
		z = UI["LabelZ"].Value
		s = UI["LabelName"].Text
		UI.Call("AddLabel", x, y, z, s)

	def RemoveLabel_ButtonClickEvent(o):
		selected = UI["labels"].SelectedItem
		if selected:
			x = selected.Attributes["x"]
			y = selected.Attributes["y"]
			z = selected.Attributes["z"]
			UI.Call("RemLabel", x, y, z)

	def labels_ItemSelectedEvent(item):
		if item:
			x = item.Attributes["x"]
			y = item.Attributes["y"]
			z = item.Attributes["z"]
			UI.Call("SelectLabel", x, y, z)

	def UpdateLabels(points):
		UI["labels"].Clear()

		for pt in points:
			_addLabel(pt.Pos.X, pt.Pos.Y, pt.Pos.Z, pt.Label)

	def _addLabel(x, y, z, s):
		label = UI.Create("label")
		label.Height = 25
		label.Text = "($x,$y,$z): $s"
		label.Attributes["x"] = x
		label.Attributes["y"] = y
		label.Attributes["z"] = z
		label.Attributes["s"] = s
		UI["labels"].AddItem(label)
