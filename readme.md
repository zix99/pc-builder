Builder OSS
===========

This is the sandbox builder game for Empeld.  It is a mode that contains multiple
worlds for playing with, along with a basic sandbox-style build mode.

This has been released as an open source project for the community to reference as an example
and build upon.

Using
=====

In order to use this mod, you'll need a copy of Empeld.  You can grab it over at http://www.empeld.com

Building
========

1) To build, simply put a copy of empeld in a "empeld-core" subfolder next to the code.  This folder can be a symlink/junction to a release copy of Empeld.
2) Create a symlink/junction: empeld-core/plugins/builder => builder_content
3) Verify that the built version of builder goes to empeld-core/plugins/
4) Run the builder plugin with arguments to `empeld.exe`

```
--server-save=builder -bgserver -join=localhost -plugins essentials,essentials.action,essentials.blocks.vegetation,builder --world=EarthWorld2 --game=Creative --environment=Basic
```

Licensing
=========
This plugin is released under the MIT license.  If you modify it, I encourage you to re-release
it to the community, or even file a pull request to get it into this branch, but you
aren't required to.
